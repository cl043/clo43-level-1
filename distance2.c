#include<stdio.h>
#include<math.h>
float input()
{
	float x;
	printf("Enter the coordinate of the point\n");
	scanf("%f",&x);
    return x;
}
float dist(float a, float b,float c,float d)
{
	float dist;
	dist=sqrt((c-a)*(c-a)+(d-b)*(d-b));
	return dist;
}
void output (float a, float b,float c, float d,float dist)
{
	printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f\n",a,b,c,d,dist);
}

int main()
{
	float a,b,c,d,e;
	a=input();
	b=input();
	c=input();
	d=input();
	e=dist(a,b,c,d);
	output(a,b,c,d,e);
	return 0;
}

