#include<stdio.h>
#include<math.h>
struct point
{
	float x;
	float y;
	float z;
};
typedef struct point Point;
Point input()
{
	Point p;
	printf("Enter the X coordinate\n");
	scanf("%f",&p.x);
	printf("Enter the Y coordinate\n");
	scanf("%f",&p.y);
	printf("Enter the Z coordinate\n");
	scanf("%f",&p.z); 
	return p;
}
float compute(Point p1,Point p2)
{
	float dist;
	dist=sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z));
	return dist;
}
void output(Point p1,Point p2,float distance)
{
	printf("The distance between (%.2f,%.2f,%.2f) and (%.2f,%.2f,%.2f) is %.2f\n",p1.x,p1.y,p1.z,p2.x,p2.y,p2.z,distance);
}

int main()
{
	Point p1,p2;
	float distance;
	p1=input();
	p2=input();
	distance=compute(p1,p2);
	output(p1,p2,distance);
	return 0;
	
}
	
