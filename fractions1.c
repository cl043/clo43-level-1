#include <stdio.h>

struct fract
{
    int n;
    int d;
};

typedef struct fract Fract;

Fract input()
{
    Fract a;
    printf("\nEnter the numerator:\n");
    scanf("%d",&a.n);
    printf("Enter the denominator:\n");
    scanf("%d",&a.d);
    return a;
}

Fract compute(Fract a, Fract b)
{
    Fract c;
    int i,gcd;
    c.n=a.n*b.d+b.n*a.d;
    c.d=a.d*b.d;
    for(i=1;i<=c.n && i<=c.d;++i)
    {
        if(c.n%i==0 && c.d%i==0)
        gcd=i;
    }
    c.n=c.n/gcd;
    c.d=c.d/gcd;
    return c;
}

void output(Fract a, Fract b, Fract c)
{
    printf("\n%d/%d + %d/%d = %d/%d \n",a.n,a.d,b.n,b.d,c.n,c.d);
}

void main()
{
    Fract a,b,c;
    a=input();
    b=input();
    c=compute(a,b);
    output(a,b,c);
}