#include <stdio.h>
int input()
{
    int a; 
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

int enter()
{
	int b;
	printf("Enter another number\n");
	scanf("%d",&b);
	return b;
}

int sum(int a, int c, int b)
{
    int sum;
    sum = a+b+c;
    return sum;
}

void output(int a, int b, int g, int c)
{
    printf("Sum of %d %d and %d is %d\n",a,b,g,c);
}

int main()
{
    int x,y,z,g;
    x=input();
    g=enter();
    y=enter();
    z=sum(x,g,y);
    output(x,g,y,z);
    return 0;
}
