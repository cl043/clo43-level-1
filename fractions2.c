#include <stdio.h>

void input(int *n)
{
    printf("Enter The Number Of Fractions To Be Added: ");
    scanf("%d",&*n);
}

void input_array(int n, int a[n][2])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("\nEnter The Numerator Of Fraction %d: ",i+1);
        scanf("%d",&a[i][0]);
        printf("Enter The Denominator Of Fraction %d: ",i+1);
        scanf("%d",&a[i][1]);
    }
}

void compute(int n,int a[n][2],int *sn,int *sd)
{
    int i,num,den,gcd;
    num=*sn;
    den=*sd;
    
    for(i=0;i<n;i++)
    {
        num=(num*a[i][1])+(den*a[i][0]);
        den=(den)*(a[i][1]);
    }
    
    for(i=1;i<=num && i<=den;++i)
    {
        if(num%i==0 && den%i==0)
        gcd=i;
    }
    
    *sn=num/gcd;
    *sd=den/gcd;
}

void output(int n,int a[n][2],int sn,int sd)
{
    int i;
    printf("\n");
    for(i=0;i<n-1;i++)
    {
        printf("(%d/%d)+",a[i][0],a[i][1]);
    }
    printf("(%d/%d) = (%d/%d) \n",a[n-1][0],a[n-1][1],sn,sd);
}

int main ()
{
    int n;
    input(&n);
    
    int a[n][2];
    input_array(n,a);
    
    int sn=0,sd=1;
    compute(n,a,&sn,&sd);
    output(n,a,sn,sd);
    
    return 0;
}

